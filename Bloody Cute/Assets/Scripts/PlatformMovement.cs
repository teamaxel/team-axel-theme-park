﻿using UnityEngine;
using System.Collections;

public class PlatformMovement : MonoBehaviour
{

    private Rigidbody2D platformRigidbody;

    [SerializeField]
    private KeyCode jumpKey;

    [SerializeField]
    private float jumpForce;

    private bool jump;

    private bool jumpo;

    private GameObject objecto;

    // Use this for initialization
    void Start()
    {
        platformRigidbody = GetComponent<Rigidbody2D>();

        objecto = GetComponent<GameObject>();

        jumpo = Input.GetKeyDown(jumpKey);

        jumpo = true;
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        HandleInput();

        HandleMovement(0f);

    }

    private void HandleInput()
    {
        if (jumpo == true)
        {
            jump = true;
            jumpo = false;
        }
    }


    private void HandleMovement(float horizontal)
    {
        platformRigidbody.AddForce(new Vector2(horizontal, 0));

        if (jump)
        {
            platformRigidbody.AddForce(new Vector2(0, jumpForce));
            jump = false;
        }
    }
}
