﻿using UnityEngine;
using System.Collections;

public class CollisionPoint : MonoBehaviour {

    private Rigidbody2D myRigidbody;

    [SerializeField]
    private KeyCode leftKey;
    [SerializeField]
    private KeyCode rightKey;
    [SerializeField]
    private KeyCode jumpKey;

    public float knockBack;
    public float knockBackLength;
    public float knockBackCount;
    public bool knockFromRight;

    [SerializeField]
    private float movementSpeed;

    private float moveVelocity;

    [SerializeField]
    private Transform[] groundPoint;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    public LayerMask whatIsGround;
    public LayerMask whatIsGantry;
    public LayerMask whatIsPlatform;
    //private bool isGrounded;
    private bool jump;

    [SerializeField]
    private float jumpForce;

    public AudioSource jumpSound;
    bool isGrounded;
    bool isGantry;
    bool isOnPlatform;
    public float jumpCooldown = 0.25f;
    private float jumpTimer = 0f;
    public float MaxVelocity;
    // Use this for initialization
    void Start ()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () { }

    private void HandleInput()
    {
        if(jumpTimer > jumpCooldown)
        {
            if (Input.GetKey(jumpKey))
            {
                if(isGrounded || isOnPlatform)
                jump = true;
            }
        }

    }

    private void HandleMovement(float horizontal)
    {
        myRigidbody.AddForce(new Vector2(horizontal, 0));

        if (jump)
        {
            myRigidbody.AddForce(new Vector2(0, jumpForce));
            jumpSound.Play();
            jump = false;
        }
    }

    void FixedUpdate ()
    {
        jumpTimer += Time.deltaTime;
        isGrounded = Physics2D.OverlapCircle(transform.position, 0.7f, whatIsGround);
        isGantry = Physics2D.OverlapCircle(transform.position, 0.7f, whatIsGantry);

        HandleInput();

        HandleMovement(moveVelocity);

        ResetValues();

        isGrounded = IsGrounded();

        isOnPlatform = IsOnPlatform();

        moveVelocity = 0f;
        
        float h = Input.GetAxis("Horizontal") * movementSpeed;

        myRigidbody.AddForce(Vector2.right * h);

        if (Input.GetKey(leftKey))
        {
            moveVelocity = -movementSpeed;
        }

        if (Input.GetKey(rightKey))
        {
            moveVelocity = movementSpeed;
        }        
        if(isGantry)
        {
            movementSpeed = 0;
        }
        else
        {
            movementSpeed = 5;
        }

        myRigidbody.velocity = Vector3.ClampMagnitude(myRigidbody.velocity, MaxVelocity);
    }
    

    private void ResetValues()
    {
    }

    private bool IsGrounded()
    {
        RaycastHit2D ray = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, 0.7f, whatIsGround);
        if(ray.collider != null)
        {
            return true;
        }
       
        return false;
    }
    private bool IsOnPlatform()
    {
        RaycastHit2D ray = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.down, 0.7f, whatIsPlatform);
        if (ray.collider != null)
        {
            return true;
        }

        return false;
    }

    //        foreach (Transform point in groundPoint)
    //        {
    //            Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);

    //            for ( int i = 0; i < colliders.Length; i++)
    //            {
    //                if(colliders[i].gameObject != gameObject)
    //                {
    //                    return true;
    //                }
    //            }
    //        }
    //    }
    //    return false;
    //}


}
