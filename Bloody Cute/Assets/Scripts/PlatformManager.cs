﻿using UnityEngine;
using System.Collections;

public class PlatformManager : MonoBehaviour {

    public Transform[] SpawnPoint;

    public float spawnTime = 1.5f;

    public GameObject Platforms;



    // Use this for initialization
    void Start ()
    {
        InvokeRepeating("SpawnPlatforms", spawnTime, spawnTime);
    }

    // Update is called once per frame
    void Update () {}

    void SpawnPlatforms()
    {
        int SpawnIndex = Random.Range(0, SpawnPoint.Length); // set the index number of the array randomly
        Instantiate(Platforms, SpawnPoint[SpawnIndex].position, SpawnPoint[SpawnIndex].rotation);
    }

}
